<?php
// the url to scrap from
$main_url = 'https://jobs.sanctuary-group.co.uk';
$html = file_get_contents($main_url . '/search/'); //get the html returned from the following url
// create a DOM document
$document = new DOMDocument();

libxml_use_internal_errors(TRUE); //disable libxml errors

if(!empty($html)){ //if any html is actually returned
    // load the returned html into the DOM
	$document->loadHTML($html);
	libxml_clear_errors(); //remove errors for inline html
	// parse the document to XPath 
	$dom_xpath = new DOMXPath($document);

	//get all the links
    $main_row = $dom_xpath->query('//a[@class="jobTitle-link"]');
    // there are two links one visible and one hidden
    // the variable to check so we dont do this twice
    $rowsProcessed = 0;
    // the arrays to hold all the scraped values
    $links = array();
    $jobTitles = array();
    $carehomes = array();
    $locations = array();
    $departments = array();
    $operations = array();
    $req_numbers = array();
    $salaries = array();
    $descriptions = array();
    $closing_dates = array();
    // get the links and the job titles
	if($main_row->length > 0){
		foreach($main_row as $row){
            if($rowsProcessed == 0){
                array_push($links, $row->getAttribute('href'));
                array_push($jobTitles, $row->nodeValue);
                $rowsProcessed++;
            }
            else{
                $rowsProcessed = 0;
            }
		}
    }
    // get the locations
    $main_row = $dom_xpath->query('//td[@headers="hdrLocation"]');
    if($main_row->length > 0){
		foreach($main_row as $row){
            $locs = $dom_xpath->query('span[@class="jobLocation"]', $row);
            foreach($locs as $location){
                array_push($locations, $location->nodeValue);
            }
        }
    }
    // get department
    $main_row = $dom_xpath->query('//td[@headers="hdrDepartment"]');
    if($main_row->length > 0){
		foreach($main_row as $row){
            $deps = $dom_xpath->query('span[@class="jobDepartment"]', $row);
            foreach($deps as $department){
                array_push($departments, $department->nodeValue);
            }
        }
    }
    // get the operations
    $main_row = $dom_xpath->query('//td[@headers="hdrFacility"]');
    if($main_row->length > 0){
		foreach($main_row as $row){
            $ops = $dom_xpath->query('span[@class="jobFacility"]', $row);
            foreach($ops as $operation){
                array_push($operations, $operation->nodeValue);
            }
        }
    }
    
}
//now get each link...and get other stuff like the req numbers, closing dates

for($i=0; $i<count($links); $i++){
    $html = file_get_contents($main_url . $links[$i]); //get the html returned from the following url
    $document = new DOMDocument();
    libxml_use_internal_errors(TRUE); //disable libxml errors

    if(!empty($html)){ //if any html is actually returned

	    $document->loadHTML($html);
	    libxml_clear_errors(); //remove errors for inline stuffed html
        $dom_xpath = new DOMXPath($document);
        // get the requisition numbers
        $main_row = $dom_xpath->query('//span[@itemprop="customfield5"]');
        if($main_row->length > 0){
            foreach($main_row as $row){
                array_push($req_numbers, $row->nodeValue);
            }
        }
        // the others are all within the jobdescription span
        $main_row = $dom_xpath->query('//span[@class="jobdescription"]');
        if($main_row->length > 0){
            foreach($main_row as $row){
                // description
                $descs = $dom_xpath->query('p[2]', $row);
                foreach($descs as $description){
                    array_push($descriptions, $description->nodeValue); 
                }
                // the care home names
                $chomes = $dom_xpath->query('p[6]', $row);
                foreach($chomes as $chome){
                    array_push($carehomes, $chome->nodeValue); 
                }
                // the salary rates
                $rates = $dom_xpath->query('p[8]', $row);
                foreach($rates as $rate){
                    array_push($salaries, $rate->nodeValue); 
                }
                // the closing dates are in a paragraph inside this
                $cdates = $dom_xpath->query('//p', $row);
                foreach($cdates as $date){
                    // and it has the text 'Closing Date:' or 'Closing date:'
                    if(strpos($date->nodeValue, 'Closing Date:') !== false || strpos($date->nodeValue, 'Closing date:') !== false){
                        array_push($closing_dates, $date->nodeValue); 
                    }
                }
            }
        }
    }
}
// now write the CSV
$list = array (
    $jobTitles,
    $carehomes,
    $locations,
    $departments,
    $operations,
    $req_numbers,
    $salaries,
    $closing_dates,
    $descriptions
  );
  
  $file = fopen("scrapped.csv","w");
  
  foreach ($list as $line) {
    fputcsv($file, $line);
  }
  
  fclose($file);
?>